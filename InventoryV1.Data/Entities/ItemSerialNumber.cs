﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryV1.Data.Entities
{
    public class ItemSerialNumber
    {
        public int Id { get; set; }
        public string SerialNumber { get; set; }

        //public virtual IEnumerable<Item> Items { get; set; }
        public virtual Batch Batches { get; set; }
    }
}
