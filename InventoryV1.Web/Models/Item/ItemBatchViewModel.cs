﻿using InventoryV1.Web.Models.Batch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryV1.Web.Models.Item
{
    public class ItemBatchViewModel
    {
        public ItemListingViewModel Item { get; set; }
        public IEnumerable<BatchListingViewModel> Batches { get; set; }
    }
}
