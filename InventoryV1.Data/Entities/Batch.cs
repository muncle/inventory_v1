﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryV1.Data.Entities
{
    public class Batch
    {
        public int Id { get; set; }
        public string GoodsReceivedNote { get; set; }
        public DateTime ExpiryDate { get; set; }

        public virtual Item Items { get; set; }
        public virtual IEnumerable<ItemSerialNumber> ItemSerialNumbers { get; set; }
    }
}
