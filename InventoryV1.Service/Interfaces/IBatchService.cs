﻿using InventoryV1.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryV1.Service.Interfaces
{
    public interface IBatchService
    {
        Batch GetById(int id);
        IEnumerable<Batch> GetAll();
        IEnumerable<Batch> GetFilteredBatches(string searchQuery);
        IEnumerable<Batch> GetBatchesByItem(int id);

        Task Create(Batch batch);
        Task Delete(int id);
        Task UpdateGoodsReceivedNote(int id, string newGoodsReceivedNote);
        Task UpdateExpiryDate(int id, string newExpiryDate);

    }
}
