﻿using InventoryV1.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryV1.Service.Interfaces
{
    public interface IItemService
    {
        Item GetById(int id);
        IEnumerable<Item> GetAll();

        Task Create(Item item);
        Task Delete(int itemId);
        Task UpdateItemPartNumber(int itemId, string newPartNumber);
        Task UpdateItemDescription(int itemId, string newItemDescription);
        Task UpdateItemIsActive(int itemId, string newIsActive);

    }
}
