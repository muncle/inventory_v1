﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryV1.Web.Models.Item
{
    public class ItemIndexViewModel
    {
        public IEnumerable<ItemListingViewModel> ItemList { get; set; }
    }
}
