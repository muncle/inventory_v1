﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryV1.Data.Entities
{
    public class Store
    {
        public int Id { get; set; }
        public string StoreCode { get; set; }
        public string StoreName { get; set; }
    }
}
