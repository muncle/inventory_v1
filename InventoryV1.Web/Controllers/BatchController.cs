﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryV1.Service.Interfaces;
using InventoryV1.Web.Models.Batch;
using Microsoft.AspNetCore.Mvc;

namespace InventoryV1.Web.Controllers
{
    public class BatchController : Controller
    {
        private readonly IBatchService _batchService;

        public BatchController(IBatchService batchService)
        {
            _batchService = batchService;
        }

        public IActionResult Index(int id)
        {
            var batch = _batchService.GetById(id);

            var model = new BatchIndexViewModel
            {
                Id = batch.Id,
                GoodsReceivedNote = batch.GoodsReceivedNote,
                ExpiryDate = batch.ExpiryDate,
                //Item = batch.Item.PartNumber

            };

            return View(model);
        }
    }
}