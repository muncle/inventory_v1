﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryV1.Web.Models.Item
{
    public class ItemDetailViewModel
    {
        public int Id { get; set; }
        public string PartNumber { get; set; }
        public string ItemDescription { get; set; }
        public bool IsActive { get; set; }

        public ItemListingViewModel Item { get; set; }
    }
}
