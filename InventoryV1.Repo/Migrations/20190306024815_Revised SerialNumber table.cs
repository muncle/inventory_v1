﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace InventoryV1.Repo.Migrations
{
    public partial class RevisedSerialNumbertable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Batches_ItemSerialNumbers_ItemSerialNumberId",
                table: "Batches");

            migrationBuilder.DropIndex(
                name: "IX_Batches_ItemSerialNumberId",
                table: "Batches");

            migrationBuilder.DropColumn(
                name: "ItemSerialNumberId",
                table: "Batches");

            migrationBuilder.AddColumn<int>(
                name: "BatchesId",
                table: "ItemSerialNumbers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ItemSerialNumbers_BatchesId",
                table: "ItemSerialNumbers",
                column: "BatchesId");

            migrationBuilder.AddForeignKey(
                name: "FK_ItemSerialNumbers_Batches_BatchesId",
                table: "ItemSerialNumbers",
                column: "BatchesId",
                principalTable: "Batches",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ItemSerialNumbers_Batches_BatchesId",
                table: "ItemSerialNumbers");

            migrationBuilder.DropIndex(
                name: "IX_ItemSerialNumbers_BatchesId",
                table: "ItemSerialNumbers");

            migrationBuilder.DropColumn(
                name: "BatchesId",
                table: "ItemSerialNumbers");

            migrationBuilder.AddColumn<int>(
                name: "ItemSerialNumberId",
                table: "Batches",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Batches_ItemSerialNumberId",
                table: "Batches",
                column: "ItemSerialNumberId");

            migrationBuilder.AddForeignKey(
                name: "FK_Batches_ItemSerialNumbers_ItemSerialNumberId",
                table: "Batches",
                column: "ItemSerialNumberId",
                principalTable: "ItemSerialNumbers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
