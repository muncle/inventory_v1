﻿using InventoryV1.Data.Entities;
using InventoryV1.Repo;
using InventoryV1.Service.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryV1.Service
{
    public class BatchService : IBatchService
    {
        private readonly ApplicationDbContext _context;

        public BatchService(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task Create(Batch batch)
        {
            throw new NotImplementedException();
        }

        public Task Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Batch> GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Batch> GetBatchesByItem(int id)
        {
            return _context.Items
                .Where(Item => Item.Id == id).First()
                .Batches;
        }

        public Batch GetById(int id)
        {
            return _context.Batches.Where(batch => batch.Id == id)
                //.Include(batch => batch.GoodsReceivedNote)
                //.Include(batch => batch.ExpiryDate)
                .First();
        }

        public IEnumerable<Batch> GetFilteredBatches(string searchQuery)
        {
            throw new NotImplementedException();
        }

        public Task UpdateGoodsReceivedNote(int id, string newGoodsReceivedNote)
        {
            throw new NotImplementedException();
        }

        public Task UpdateExpiryDate(int id, string newExpiryDate)
        {
            throw new NotImplementedException();
        }
    }
}
