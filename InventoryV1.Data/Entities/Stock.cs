﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryV1.Data.Entities
{
    public class Stock
    {
        public int Id { get; set; }
        public int QuantityOnHand { get; set; }
        public DateTime Created { get; set; }

        public virtual IEnumerable<StockStoreLocation> Locations { get; set; }
    }
}
