﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryV1.Data.Entities
{
    public class Item
    {
        public int Id { get; set; }
        public string PartNumber { get; set; }
        public string ItemDescription { get; set; }
        public bool IsActive { get; set; }

        public virtual IEnumerable<Batch> Batches { get; set; }
    }
}
