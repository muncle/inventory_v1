﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryV1.Data.Entities
{
    public class StockStoreLocation
    {
        public int Id { get; set; }
        public int ItemLocation { get; set; }

        public virtual Stock Stock { get; set; }
        public virtual IEnumerable<Store> Stores { get; set; }
    }
}
