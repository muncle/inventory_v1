﻿using InventoryV1.Data.Entities;
using InventoryV1.Service.Interfaces;
using InventoryV1.Web.Models.Batch;
using InventoryV1.Web.Models.Item;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace InventoryV1.Web.Controllers
{
    public class ItemController : Controller
    {
        private readonly IItemService _itemService;
        private readonly IBatchService _batchService;

        public ItemController(IItemService itemService)
        {
            _itemService = itemService;
        }

        public IActionResult Index()
        {
            var items = _itemService.GetAll()
                .Select(item => new ItemListingViewModel
                {
                    Id = item.Id,
                    PartNumber = item.PartNumber,
                    ItemDescription = item.ItemDescription
                });

            var model = new ItemIndexViewModel
            {
                ItemList = items
            };

            return View(model);
        }
        
        /*
        public IActionResult Detail(int id)
        {
            var items = _itemService.GetById(id)
                .Select(item => new ItemDetailViewModel
                 {
                     Id = item.Id,
                     PartNumber = item.PartNumber,
                     ItemDescription = item.ItemDescription
                     IsActive = item.IsActive
                 });

            var model = new ItemIndexViewModel
            {
                ItemList = items
            };

            return View(model);
        } */

        public IActionResult Batch(int id)
        {
            var item = _itemService.GetById(id);
            var batches = item.Batches;

            var batchListings = batches.Select(batch => new BatchListingViewModel
            {
                Id = batch.Id,
                GoodsReceivedNote = batch.GoodsReceivedNote,
                ExpiryDate = batch.ExpiryDate,
                Item = BuildItemListing(batch)
            });

            var model = new ItemBatchViewModel
            {
                Batches = batchListings,
                Item = BuildItemListing(item)

            };

            return View(model);
        }

        private ItemListingViewModel BuildItemListing(Batch batch)
        {
            var item = batch.Items;
            return BuildItemListing(item);

        }

        private ItemListingViewModel BuildItemListing(Item item)
        {
            return new ItemListingViewModel
            {
                Id = item.Id,
                PartNumber = item.PartNumber,
                ItemDescription = item.ItemDescription
            };
        }
    }
}