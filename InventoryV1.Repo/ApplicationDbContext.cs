﻿using InventoryV1.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace InventoryV1.Repo
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Batch> Batches { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<ItemSerialNumber> ItemSerialNumbers { get; set; }
        public DbSet<Stock> Stock { get; set; }
        public DbSet<StockStoreLocation> StockStoreLocations { get; set; }
        public DbSet<Store> Stores { get; set; }
    }
}
