﻿using InventoryV1.Data.Entities;
using InventoryV1.Repo;
using InventoryV1.Service.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryV1.Service
{
    public class ItemService : IItemService
    {
        private readonly ApplicationDbContext _context;

        public ItemService(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task Create(Item item)
        {
            throw new NotImplementedException();
        }

        public Task Delete(int itemId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Item> GetAll()
        {
            return _context.Items
                .Include(item => item.Batches);
        }

        public Item GetById(int id)
        {
            var items = _context.Items.Where(item => item.Id == id)
                //TODO add more items ref video 5 - 19 minutes
                .Include(item => item.Batches)
                //.Include(item => item.ItemSerialNumber)
                .FirstOrDefault();

            return items;
        }

        public Task UpdateItemDescription(int itemId, string newItemDescription)
        {
            throw new NotImplementedException();
        }

        public Task UpdateItemIsActive(int itemId, string newIsActive)
        {
            throw new NotImplementedException();
        }

        public Task UpdateItemPartNumber(int itemId, string newPartNumber)
        {
            throw new NotImplementedException();
        }
    }
}
