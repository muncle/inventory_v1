﻿using InventoryV1.Web.Models.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryV1.Web.Models.Batch
{
    public class BatchIndexViewModel
    {
        public int Id { get; set; }
        public string GoodsReceivedNote { get; set; }
        public DateTime ExpiryDate { get; set; }

        //public int ItemId { get; set; }
        public ItemListingViewModel Item { get; set; }
    }
}
